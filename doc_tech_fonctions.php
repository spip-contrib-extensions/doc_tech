<?php
/**
 * Fonctions utiles au plugin Documentation technique
 *
 * @plugin     Documentation technique
 * @copyright  2013-2017
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Doc_tech\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lister tous les objets ayant une chaine de langue de type lang/objet_lang.php
 * Exemple : `lang/projet_fr.php`
 * `lang/forum_fr.php`
 *
 * @return array
 */
function doc_tech_lister_objet() {
	include_spip('base/objets');
	include_spip('inc/config');
	$objets_principales = array_keys(lister_tables_principales());
	// On va prendre la langue du site comme référence pour la langue de l'objet
	$langue_site = lire_config('langue_site');
	$liste_objet = array();

	foreach ($objets_principales as $objet) {
		$type = objet_type($objet);
		// On recherche les objets ayant une chaîne de langue selon le type
		// Exemple : lang/forum_fr.php
		// lang/projet_fr.php
		$lang = find_in_path("lang/" . $type . "_" . $langue_site . ".php");
		if ($lang) {
			$liste_objet[] = $type;
		}
	}

	return $liste_objet;
}


function doc_tech_chaine_langue($objet, $champ, $sufix, $sufix_obligatoire = false) {
	$langue_site = $GLOBALS['meta']['langue_site'];
	$traduction = '';
	include_spip('inc/traduire');
	charger_langue($langue_site, $objet);
	/*
	echo "<pre>" . print_r($objet, true) . "\n"
		. print_r($champ, true) . "\n"
		. print_r($sufix, true) . "\n"
		. "</pre>";
	*/
	/**
	 * Dans certains cas, une chaine de langue est bien trouvée par cette fonction. cf. page ?exec=doc_tech
	 * Et dans d'autres cas, cf. ?exec=doc_tech_lang, la chaine de langue 'correcte' n'est pas trouvée. Exemple: objet pays (avec le plugin Pays)
	 */
	$traduction = doc_tech_chercher_trad($objet . '_' . $langue_site, $langue_site, $objet, $champ, $sufix, $sufix_obligatoire);

	if (strlen($traduction) == 0) {
		foreach (doc_tech_liste_modules_actifs() as $module) {
			$traduction = doc_tech_chercher_trad($module . '_' . $langue_site, $langue_site, $module, $champ, $sufix, $sufix_obligatoire);
			if (strlen($traduction) == 0) {
				$traduction = doc_tech_chercher_trad($module . '_' . $langue_site, $langue_site, $module, $objet . '_' . $champ, $sufix, $sufix_obligatoire);			
			}
		}
	}

	return $traduction;
}

function doc_tech_chercher_trad($i18n_index, $langue_site, $objet, $champ, $sufix, $sufix_obligatoire = false) {
	include_spip('inc/traduire');
	charger_langue($langue_site, $objet);
	$traduction = '';
	if (isset($GLOBALS['i18n_' . $i18n_index])) {
		if (isset($GLOBALS['i18n_' . $i18n_index]['champ_' . $champ . '_' . $sufix])) {
			$traduction = $GLOBALS['i18n_' . $i18n_index]['champ_' . $champ . '_' . $sufix];
		} else if (isset($GLOBALS['i18n_' . $i18n_index][$champ . '_' . $sufix])) {
			$traduction = $GLOBALS['i18n_' . $i18n_index][$champ . '_' . $sufix];
		} else if (isset($GLOBALS['i18n_' . $i18n_index][$sufix . '_' . $champ])) {
			$traduction = $GLOBALS['i18n_' . $i18n_index][$sufix . '_' . $champ];
		} else if (isset($GLOBALS['i18n_' . $i18n_index][$champ]) and $sufix_obligatoire == false) {
			$traduction = $GLOBALS['i18n_' . $i18n_index][$champ];
		}
	}

	return $traduction;
}

function doc_tech_liste_modules_actifs() {
	include_spip('plugins/installer');
	$liste_plugin_actifs = liste_plugin_actifs();
	$liste_plugin_actifs = array_keys($liste_plugin_actifs);
	sort($liste_plugin_actifs);
	foreach ($liste_plugin_actifs as $index => $module) {
		if (preg_match("/:/", $module)) {
			unset($liste_plugin_actifs[$index]);
		} else {
			$liste_plugin_actifs[$index] = strtolower($module);
		}

	}
	$liste_plugin_actifs = array_values($liste_plugin_actifs);

	return $liste_plugin_actifs;
}

function doc_tech_spip_tables_field() {
	$lister_tables_principales = lister_tables_principales();
	$lister_tables_principales_field = array();

	foreach($lister_tables_principales as $spip_tables => $valeurs) {
		if (array_key_exists('field', $valeurs)) {
			$lister_tables_principales_field[$spip_tables] = $valeurs['field'];
		}
	}

	return $lister_tables_principales_field;
}