<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aide_a_la_saisie' => 'Aide à la saisie :',

	// C
	'chaines_a_placer_dans' => 'Chaînes de langue à placer dans',

	// D
	'definition_mysql' => 'Définition MySQL',
	'doc_tech_titre' => 'Documentation technique',
	'doc_tech_explication' => 'Cette page vous permet de lister les champs de plusieurs objets spip et la documentation associée.<br/> Cette documentation est réalisée grâce aux chaînes de langue de votre objet éditorial.',
	'doc_tech_lang_titre' => 'Générer les chaînes de langue',
	'doc_tech_lang_explication' => 'Cette page va vous permettre de générer les chaînes de langue pour votre plugin. Il ne crée pas les fichiers actuellement. Il vous faudra faire un <strong>copier-coller</strong> du texte généré ci-dessous dans votre fichier de langue et de le compléter le cas échéant.',
	'documentation' => 'Documentation :',
	'documentations' => 'Documentations',

	// L
	'label_label' => 'Label',
	'label_perso' => 'Des préfixes de chaînes de langue personnalisés, séparés d\'une virgule :',
	'label_regrouper_champs' => 'Regrouper les champs ?',
	'label_vos_objets' => 'Vos objets :',

	// N
	'nom_du_champ' => 'Nom du champ',

	// P
	'pas_de_tables_nom'=> 'Il n\'y a pas de tables correspondantes',

	// S 
	'sommaire' => 'Sommaire', 
	
	// V
	'value_submit_generer' => 'Générer',
);

?>