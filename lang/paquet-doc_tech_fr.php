<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'doc_tech_description' => 'Construire une page listant les champs d\'une table et y donner :
- le nom du champ ;
- sa définition MySQL ;
- son label ;
- sa documentation technique.',
	'doc_tech_nom' => 'Documentation technique',
	'doc_tech_slogan' => 'Documentons notre code!',
);

?>